//
//  ViewController.swift
//  MyApp
//
//  Created by Mavin on 9/7/20.
//  Copyright © 2020 hrd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    var firstValue: Double = 0.0;
    var secondeValue: Double = 0.0;
    var operatorButton: String = "";
    var result: Double = 0.0
    var hasOperator: Bool = false
    var hasDot: Bool = false
    var oldValue: String = "0"
    
    @IBOutlet weak var lblShowNumber: UILabel!
    @IBOutlet weak var lblShowOperator: UILabel!
    @IBOutlet weak var lblShowOldValue: UILabel!
    
    @IBAction func btnNumber(_ sender: UIButton) {
        
        var getValue:String = ""
        if(hasOperator){
            getValue = sender.currentTitle!
            hasOperator = false
        }
        else{
            getValue = (lblShowNumber.text! == "0" ? sender.currentTitle! : lblShowNumber.text! + sender.currentTitle!)
        }
        checkDot(strResult: getValue)
        lblShowNumber.text = getValue.count < 11 ? getValue : lblShowNumber.text!
    }
    
    @IBAction func btnCalculate(_ sender: UIButton) {
        
        lblShowOperator.text = sender.currentTitle!
        operatorButton = sender.currentTitle!
        hasOperator = true
        if(firstValue > 0){
            secondeValue = Double(lblShowNumber.text!)!
            switch operatorButton {
            case "+":
                firstValue = firstValue + secondeValue
            case "-":
                firstValue = firstValue - secondeValue
            case "x":
                firstValue = firstValue * secondeValue
            case "/":
                firstValue = firstValue / secondeValue
            case "%":
                firstValue = Double(firstValue).truncatingRemainder(dividingBy: Double(secondeValue))
            default:
                print("default")
            }
            
            let firstValueStr = String(firstValue)
            let dataFirstValue = firstValueStr.components(separatedBy: ".")
            
            lblShowNumber.text = dataFirstValue[1] == "0" ? dataFirstValue[0] : firstValueStr
            
        }else {
            firstValue = Double(lblShowNumber.text!)!
        }
        lblShowOldValue.text = lblShowNumber.text!
        checkDot(strResult: lblShowNumber.text!)
    }
    @IBAction func btnDot(_ sender: UIButton) {
        lblShowNumber.text = hasDot ? lblShowNumber.text! : lblShowNumber.text! + sender.currentTitle!
        hasDot = true
    }
    @IBAction func btnSumMinus(_ sender: UIButton) {
        var valueToSumMinus = lblShowNumber.text!
        valueToSumMinus = String(Double(valueToSumMinus)! * (-1))
        let numbers = valueToSumMinus.components(separatedBy: ".")
        valueToSumMinus = numbers[1] > "0" ? valueToSumMinus : numbers[0]
        lblShowNumber.text = valueToSumMinus
    }
    
    @IBAction func btnResult(_ sender: UIButton) {
        
       lblShowOldValue.text = lblShowNumber.text!
       secondeValue = Double(lblShowNumber.text!)!
        
       switch operatorButton {
       case "+":
           result = firstValue + secondeValue
       case "-":
           result = firstValue - secondeValue
       case "x":
           result = firstValue * secondeValue
       case "÷":
           result = firstValue / secondeValue
       case "%":
        result = Double(firstValue).truncatingRemainder(dividingBy: Double(secondeValue))
       default:
           result = secondeValue
       }

       let stringResult = [String(result) , String(firstValue)]
       let numbersResult = stringResult[0].components(separatedBy: ".")
       let numbersFirst = stringResult[1].components(separatedBy: ".")
        
       let numResult = numbersResult == ["inf"] ? "0" : numbersResult[1]
       var lastResult = result != firstValue ? numResult > "0" ? String(result) : numbersResult[0] : numbersFirst[1] > "0" ? String(firstValue) : numbersFirst[0]
        lastResult = lastResult == "inf" ? "Not a Number" : lastResult
        checkDot(strResult: lastResult)
        
        lblShowNumber.text = lastResult
        hasOperator = true
        firstValue = 0;
        secondeValue = 0;
        lblShowOperator.text = ""
        operatorButton = ""
        
    }
    
    func checkDot(strResult: String) {
        for result in strResult {
            if result == "." {
                hasDot = true
                break
            }else{
                hasDot = false
            }
        }
        
    }
    @IBAction func btnClear(_ sender: UIButton) {
       lblShowOperator.text = ""
       lblShowNumber.text = "0"
       lblShowOldValue.text = "0"
       firstValue = 0;
       secondeValue = 0;
       hasDot = false
    
    }
}
